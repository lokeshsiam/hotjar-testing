
import "./App.css";
import "react-datepicker/dist/react-datepicker.css";
import PrivateRoute from "./mainLayout/index";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter,
} from "react-router-dom";
import Login from "../src/components/Login/Login";
import Dashboard from "./pages/dashboard";
import TestPopup from "./pages/testPopup";

import MainPages from "./pages/mainPages";
import SimpleMap from "./pages/map";
import ChangeAddress from "./pages/changeAddress";
import RetailStore from "./pages/retailStore";
import ReportDeliveryPartner from "./pages/reportDeliveryPartner";
import Coupon from "./pages/coupon";
import ReportOrder from "./pages/reportOrder";
import UserDelivery from "./pages/deliverypartner";
import PopupTable from "./pages/popupTable";
// import UserDelivery from "./pages/userDelivery";
import ManageProducts from "./pages/manageProducts";
import ManageCategory from "./pages/manageCategory";
import Support from "./pages/support";

import LogoutModalPopup from "./components/logoutPopup";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={withRouter(Login)} />
          <Route exact path="/map" component={withRouter(SimpleMap)} />
          <PrivateRoute
            exact
            path="/Dashboard"
            component={withRouter(Dashboard)}
          />
          <PrivateRoute exact path="/store" component={withRouter(MainPages)} />
          <PrivateRoute
            exact
            path="/userdelivery"
            component={withRouter(UserDelivery)}
          />
          <PrivateRoute
            exact
            path="/assignretail"
            component={withRouter(PopupTable)}
          />
          {/* <PrivateRoute exact path="/inprogressorder" component={withRouter(UserDelivery)} /> */}

          <PrivateRoute exact path="/popup" component={withRouter(TestPopup)} />

          <PrivateRoute
            exact
            path="/products"
            component={withRouter(ManageProducts)}
          />
          <PrivateRoute
            exact
            path="/delivery"
            component={withRouter(ManageCategory)}
          />
          <PrivateRoute exact path="/support" component={withRouter(Support)} />

          <PrivateRoute
            exact
            path="/changeaddress"
            component={withRouter(ChangeAddress)}
          />
          <PrivateRoute
            exact
            path="/retailstore"
            component={withRouter(RetailStore)}
          />
          <PrivateRoute
            exact
            path="/reportdeliverypartner"
            component={withRouter(ReportDeliveryPartner)}
          />

          <PrivateRoute exact path="/coupon" component={withRouter(Coupon)} />
          <PrivateRoute
            exact
            path="/order"
            component={withRouter(ReportOrder)}
          />
          <PrivateRoute
            exact
            path="/logoutModal"
            component={withRouter(LogoutModalPopup)}
          />
        </Switch>
      </Router>
    </>
  );
}

export default App;

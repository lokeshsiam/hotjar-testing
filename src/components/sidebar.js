import React, { useState, useEffect } from "react";
import { NavLink, Link } from "react-router-dom";
import { Button, Accordion } from "react-bootstrap";
import Dashboard from "../assests/home-1.png";
import Dashboard2 from "../assests/home-2.png";
import User from "../assests/user-1.png";
import Report from "../assests/report-1.png";
import Manage from "../assests/product-1.png";
import Coupon from "../assests/coupon-1.png";
import Coupon2 from "../assests/coupon-2.png";
import Support from "../assests/support-1.png";
import Support2 from "../assests/support-2.png";
import Address2 from "../assests/address-2.png";
import Address from "../assests/address-1.png";
import LogOut from "../assests/logout-1.png";
import { useHistory } from "react-router-dom";

import { BiChevronDown, BiChevronUp } from "react-icons/bi";
import octoLogo from "../assests/octoLogo.png";
import { IoCloseSharp } from "react-icons/io5";
import LogoutModalPopup from "../components/logoutPopup";
import { ReactComponent as Companies } from "../assests/home.svg";
import { useNavigate, useLocation } from "react-router-dom";
export default function SideBar(props) {
  const [userArrow, setUserArrow] = useState(false);
  const [productArrow, setProductArrow] = useState(false);
  const [reportsArrow, setReportsArrow] = useState(false);
  const [logoutPopup, setLogoutPopup] = useState(false);
  const [activeLink, setActiveLink] = useState("");
  const  navigate = useHistory();

  const logoutModal = () => {
    setLogoutPopup(true);
  };
  const logout = ()=>{
    navigate.push("/");
  }
  // const navigate = useNavigate();
  const location = useLocation();
  // const onNavigate = (path) => {
  //   navigate(path);
  // };
  useEffect(() => {
    getActiveLocation();
  }, [location.pathname]);
  const getActiveLocation = () => {
    setActiveLink(location.pathname);
  };
  const userManagementAccordion = (e) => {
    setUserArrow(!userArrow);
    setReportsArrow(false);
    setProductArrow(false);
  };
  const reportAccordion = (e) => {
    setReportsArrow(!reportsArrow);
    setUserArrow(false);
    setProductArrow(false);
  };
  const productAccordion = (e) => {
    setProductArrow(!productArrow);
    setReportsArrow(false);
    setUserArrow(false);
  };
  /*---------------------Log out content------------------------ */
  const logoutContent = () => {
    return (
      <>
        <IoCloseSharp
          className="float-right  ml-1 close-icon"
          onClick={() => {
            setLogoutPopup(false);
          }}
        />
        <p className="mb-6 mt-2 areyou"> Are you sure you want to log out? </p>
        <div className="d-flex logout-double-btns mb-2 ">
          <Button
            variant="primary"
            size="sm"
            className="pr-4 pl-4 logout-no-btn"
          >
            No
          </Button>
          <Button
            // variant="secondary"
            size="sm"
            className="p-0 pr-4 pl-4 logout-yes-btn ml-4"
            onClick={logout}
          >
            Yes
          </Button>
        </div>
      </>
    );
  };
  return (
    <>
      <aside>
        <div className="sidebar nav-collapse">
          <ul>
            <li>
              <NavLink to="/dashboard" activeClassName="active">
                <div className="d-flex align-items-center">
                  {" "}
                  {/* <span>
                    <Companies />
                  </span> */}
                  {activeLink === "/dashboard" ? (
                    <img src={Dashboard2} alt="Dashboard" className="mr-3" />
                  ) : (
                    <img src={Dashboard} alt="Dashboard" className="mr-3" />
                  )}
                  <div> Dashboard</div>
                </div>
              </NavLink>
            </li>
            <li>
              <Accordion>
                <Accordion.Item eventKey="0">
                  <Accordion.Header onClick={() => userManagementAccordion()}>
                    <div className="d-flex align-items-center">
                      {" "}
                      {activeLink ? (
                        <img src={User} alt="Dashboard" className="mr-3" />
                      ) : (
                        ""
                      )}
                      <div className="mr-3">User management</div>
                      <div className="sidebar-arrow">
                        {userArrow ? (
                          <BiChevronUp size={25} />
                        ) : (
                          <BiChevronDown size={25} />
                        )}
                      </div>
                    </div>
                  </Accordion.Header>
                  <Accordion.Body>
                    <ul>
                      <li>
                        <NavLink to="/store" activeClassName="active">
                          Retail store
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/userdelivery" activeClassName="active">
                          Delivery partner
                        </NavLink>
                      </li>
                    </ul>
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                  <Accordion.Header onClick={() => reportAccordion()}>
                    <div className="d-flex  align-items-center ">
                      {activeLink ? (
                        <img src={Report} alt="Dashboard" className="mr-3" />
                      ) : (
                        ""
                      )}
                      <div className="mr-5">Reports</div>
                      <div className="sidebar-arrow">
                        {reportsArrow ? (
                          <BiChevronUp size={25} />
                        ) : (
                          <BiChevronDown size={25} />
                        )}
                      </div>
                    </div>
                  </Accordion.Header>
                  <Accordion.Body>
                    <ul>
                      <li>
                        <NavLink to="/retailstore" activeClassName="active">
                          Retail store
                        </NavLink>
                      </li>
                      <li>
                        <NavLink
                          to="/reportdeliverypartner"
                          activeClassName="active"
                        >
                          Delivery partner
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/order" activeClassName="active">
                          Order
                        </NavLink>
                      </li>
                    </ul>
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="2">
                  <Accordion.Header onClick={() => productAccordion()}>
                    <div className="d-flex align-items-center">
                      {" "}
                      {activeLink ? (
                        <img src={Manage} alt="Dashboa" className="mr-3" />
                      ) : (
                        ""
                      )}
                      <div>Manage product</div>
                      <div className="sidebar-arrow">
                        {productArrow ? (
                          <BiChevronUp size={25} />
                        ) : (
                          <BiChevronDown size={25} />
                        )}
                      </div>
                    </div>
                  </Accordion.Header>
                  <Accordion.Body>
                    <ul>
                      <li>
                        <NavLink to="/products" activeClassName="active">
                          Product
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/delivery" activeClassName="active">
                          Category
                        </NavLink>
                      </li>
                    </ul>
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
            </li>
            <li>
              <NavLink to="/coupon" activeClassName="active">
                <div className="d-flex align-items-center">
                  {" "}
                  {activeLink === "/coupon" ? (
                    <img src={Coupon2} alt="Dashbo" className="mr-3" />
                  ) : (
                    <img src={Coupon} alt="Dashboard" className="mr-3" />
                  )}
                  <div> Coupon management</div>
                </div>
              </NavLink>
            </li>
            <li>
              {/* <NavLink to="/support" activeClassName="active"> */}
              <NavLink to="/support" activeClassName="active">
                <div className="d-flex align-items-center">
                  {" "}
                  {activeLink === "/support" ? (
                    <img src={Support2} alt="Dashboard" className="mr-3" />
                  ) : (
                    <img src={Support} alt="Dashboard" className="mr-3" />
                  )}
                  <div>Support</div>
                </div>
              </NavLink>
            </li>
            <li>
              <NavLink to="/changeaddress" activeClassName="active">
                <div className="d-flex align-items-center">
                  {" "}
                  {activeLink === "/changeaddress" ? (
                    <img src={Address2} alt="Dashbo" className="mr-3" />
                  ) : (
                    <img src={Address} alt="Dashbo" className="mr-3" />
                  )}
                  <div> Change address</div>
                </div>
              </NavLink>
            </li>
            <li>
              <Link activeClassName="active" onClick={() => logoutModal()}>
                <div className="d-flex align-items-center">
                  {activeLink ? (
                    <img src={LogOut} alt="Dashboard" className="mr-3" />
                  ) : (
                    ""
                  )}
                  <div>Logout</div>
                </div>
              </Link>
            </li>
            {logoutPopup && (
              <LogoutModalPopup
                close={setLogoutPopup}
                popup={logoutPopup}
                body={logoutContent()}
                size={"modal-40w"}
              />
            )}
          </ul>
        </div>
      </aside>
    </>
  );
}

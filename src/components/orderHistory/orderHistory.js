import React from "react";
import { Row, Col } from "react-bootstrap";

export default function OrderHistory() {
  return (
    <>
      <div className="d-flex justify-content-between mr-3 ml-3 mb-1">
        <p className="color-text order-text"> Order ID : 1001 </p>
        {/* <p className="order-date"> Order Date and Time:10 jan 2022,2:00pm </p> */}
      </div>
      <div className="d-flex justify-content-between retailStore-detail  ml-3 mb-3 flex-sm">
        <p className="order-date"> Order value: 200</p>
        <p className="order-date"> Status: Delivered</p>
        <p className="order-date"> Payment Status:Completed</p>
      </div>
      <div className="splitWrapper p-3">
        <div className="lableWrapper ">
          <div className="marker mr-3 "></div>
          <div className="d-flex align-items-center flex-sm">
            <label className="text mr-3">Ordered By</label>
            <label className="date ">11 May 2021, 19 : 22</label>
          </div>
        </div>
        <div className="dividervertical w-100">
          <div className="dividerWrapper mr-4"></div>
          <div className="pt-4 pb-4 d-flex flex-column w-100">
            <div className=" pb-4 orderProfile ">
              <div className="text-size">siam</div>
              <div className="text-size">Email:siam@gmail.com</div>
              <div className="text-size">Contact no:994229673</div>
            </div>
            <div className="text-size w-50">GST Road, Adyar,<br></br> Chennai -112</div>
          </div>
        </div>
        <div className="lableWrapper">
          <div className="marker mr-3"></div>
          <div className="d-flex align-items-center flex-sm">
            <label className="text mr-3">Dispacthed by</label>
            <label className="date ">11 May 2021, 19 : 22</label>
          </div>
        </div>
        <div className="dividervertical w-100">
          <div className="dividerWrapper mr-4"></div>
          <div className="pt-4 pb-4 d-flex flex-column w-100">
            <div className=" pb-4 orderProfile">
              <div className="text-size">siam</div>
              <div className="text-size">Email:siam@gmail.com</div>
              <div className="text-size">Contact no:994229673</div>
            </div>
            <div className="text-size w-50 ">
              SAI pharmacy, Arundale coloney, Besant nagar, ch- 113
            </div>
          </div>
        </div>
        <div className="lableWrapper">
          <div className="marker mr-3"></div>
          <div className="d-flex align-items-center flex-sm">
            <label className="text mr-3">Delivered by</label>
            <label className="date ">11 May 2021, 19 : 22</label>
          </div>
        </div>
        <div className="dividervertical w-100 ">
          <div className="pt-4 pb-4 d-flex flex-column w-100 ml-4 pl-3">
            <div className=" pb-4 orderProfile ">
              <div className="text-size">siam</div>
              <div className="text-size">Email:siam@gmail.com</div>
              <div className="text-size">Contact no:994229673</div>
            </div>
            <div className="text-size">Comission received: Rs.30</div>
          </div>
        </div>
        {/* <div className="lableWrapper">
        <div className="marker  mr-3"></div>
        <label className="text">Dispacthed by</label>
      </div>
      <div className="dividerWrapper"></div>
      <div className="lableWrapper">
        <div className="marker  mr-3"></div>
        <label className="text">Delivered by</label>
      </div> */}
      </div>
    </>
  );
}

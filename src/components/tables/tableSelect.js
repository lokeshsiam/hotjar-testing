import React, {useState} from "react";
// import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
// import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
// import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import BootstrapTable from "react-bootstrap-table-next";
// import  TableHeaderColumn  from 'react-bootstrap-table';
// import paginationFactory from "react-bootstrap-table2-paginator";
// import img1 from '../assests/view.png';
// import paginationFactory from "react-bootstrap-table2-paginator";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import PaginatedItems from "../pagination/pagination";

import {
  Button,
  Form,
  Toast,
  Row,
  Col,
  FormControl,
  InputGroup,
} from "react-bootstrap";
import { Link } from "react-router-dom";

// export default function MainTable(props) {
//   return (
//     <>
//       <section>
//         <div>
//           <div className="retailHeading">
//             <h2>{props.head}</h2>
//             {/* <h2>Retail store</h2> */}
//           </div>
//         </div>

//         <div className="searchbar">
//           <form class="form-inline">
//             <input
//               class="form-control mr-sm-2 search"
//               type="search"
//               placeholder="Search"
//               aria-label="Search"
//             ></input>
//           </form>
//           {props.buttonhead ? (
//             <div className="btn-one">
//               <button type="button">
//                 {/* Add Delivery partner */}
//                 {props.buttonhead}
//               </button>
//             </div>
//           ) : null}
//         </div>

//         <div className="table-one">
//           <BootstrapTable
//             keyField="id"
//             data={props.data}
//             columns={props.columns}
//             bordered={false}
//             bootstrap4
//             wrapperClasses="table-responsive"
//             rowClasses="text-nowrap"
//           />
//         </div>
//       </section>

export default function MainTable(props) {
    const [selectRowId, setSelectRowId] = useState([]);
  //   const pagintnoptions = {
  //     // pageStartIndex: 0,
  //     sizePerPage: 15,
  //     hideSizePerPage: true,
  //     hidePageListOnlyOnePage: true,
  //   };

  //   const formatDatas = (initArr) => {
  //     let sn = 0;
  //     let filteredarray = [];

  //     initArr.forEach((element) => {
  //       const offVal = ((props.offset-1)*10)+(sn+1);
  //       const offsetValue = (props.offset ? offVal : sn+1 )

  //       filteredarray.push({
  //         ...element,
  //         sno: offsetValue,

  //         // action: element.id,
  //       });
  //       sn++;
  //     });
  //     return filteredarray;
  //   };
  // // console.log(props,'props')
  //   const data = formatDatas(props.data);
  //   const columns = props.columns;
  //   const filters = props.filters ? props.filters() : null;
   const clickSelect =  true;
  const selectRow = {
    mode: "checkbox",
    // clickToSelect: clickSelect,
    style: { background: "#eee" },
    onSelect: (row, isSelect) => {
      if (isSelect) {
        setSelectRowId((prev) => [...prev, row.id]);
      } else {
        var index = selectRowId.indexOf(row.id);
        if (index !== -1) {
          const dup = [...selectRowId];
          dup.splice(index, 1);
          setSelectRowId([...dup]);
        }
      }
    },
    onSelectAll: (isSelect, rows, e) => {
      if (isSelect) {
        rows.forEach((item) => {
          setSelectRowId((prev) => [...prev, item.id]);
        });
      } else {
        setSelectRowId([]);
      }
    },
  };

  return (
    <>
      <div className="fixedHeightTable">
        <div className="customTable mt-5">
          <div>
            <BootstrapTable
              keyField="id"
              bordered={false}
              selectRow={selectRow}
              data={props.data}
              columns={props.columns}
              bootstrap4={true}
              headerWrapperClasses="thead-dark"
              bodyClasses="tableBody"
              wrapperClasses="table-responsive customScroll"
            />
          </div>
          <div className=" pagination-content p-2">
            <div className="d-flex  align-items-center w-50">
              <label className="page">Rows per page: &nbsp;</label>
              <div>
                <Form.Control
                  as="select"
                  name="workout_category_id"
                  className="w-100"
                >
                  <option value="5">5</option>
                  <option value="10">10</option>
                  <option value="20">20</option>
                </Form.Control>
              </div>
            </div>

            <div className="pagenationwrapper d-flex ">
              <div className="text-end p-2 mr-4 page"> 1-10 of 275</div>
              <PaginatedItems
                itemsPerPage={4}
                item={props.data}
                setpages={props.setPage}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

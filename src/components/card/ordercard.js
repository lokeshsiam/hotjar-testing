import React from "react";
import { Card, Row, Col } from "react-bootstrap";
import Prescription from "../../assests/prescription.png";
import Button from "../button/button";
import Dropdown from "react-bootstrap/Dropdown";
import { Button as SplitButton } from "react-bootstrap";
import { BiChevronDown, BiChevronUp } from "react-icons/bi";
import { useState } from "react";
import ViewModalPopup from "../modalPopup";
import MainTable from "../tables/mainTable";
import { FaSearch } from "react-icons/fa";

export default function OrderCard() {
  const [manualAssign, setManualAssign] = useState(false);
  const [data, setData] = useState([
    { returId: "", orderId: 123234 },
    { returId: 0, orderId: 123234 },
  ]);
  const [viewRetailPopup, setViewRetailPopup] = useState(false);
  const [assignRetail, setAssignRetail] = useState(false);
  const assignRetailModal = () => {
    setAssignRetail(true);
  };

  /*-------------------Data for Table--------------------------- */
  const retailerLocation = [
    {
      id: 0,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 1,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 2,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 3,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
  ];

  const data1 = [
    {
      retailer_name: "Raj Kumar",
      location: "Adayar, chennai",
    },
    {
      retailer_name: "Varun",
      location: "Adayar, chennai",
    },
    {
      retailer_name: "Hema",
      location: "Adayar, chennai",
    },
    {
      retailer_name: "Dinesh",
      location: "Adayar, chennai",
    },
    {
      retailer_name: "Dheena",
      location: "Adayar, chennai",
    },
    {
      retailer_name: "Bhavana",
      location: "Adayar, chennai",
    },
    {
      retailer_name: "Sneha",
      location: "Adayar, chennai",
    },
    {
      retailer_name: "Varun",
      location: "Adayar, chennai",
    },
    {
      retailer_name: "Ajith",
      location: "Adayar, chennai",
    },
    {
      retailer_name: "Priya",
      location: "Adayar, chennai",
    },
  ];

  const columns1 = [
    {
      dataField: "retailer_name",
      text: "Retailer name",
    },
    {
      dataField: "location",
      text: "Location",
    },
  ];

  /*------------view order------------------- */
  const bodyContent = () => {
    return (
      <>
        <div className="d-flex justify-content-between mr-3 ml-3 mb-1 flex-sm">
          <p className="color-text order-text"> Order ID : 1001 </p>
          <p className="order-date"> Order Date and Time:10 jan 2022,2:00pm </p>
        </div>
        <div className="d-flex justify-content-between retailStore-detail  ml-3 mb-3 flex-sm ">
          <p className="order-date"> Customer name: Raj </p>
          <p className="order-date">
            {" "}
            Mobile: <span> 9898978938 </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Email: <span> raj@email.com </span>{" "}
          </p>
        </div>

        <div className="d-flex justify-content-between mr-3 ml-3 mb-5 product-details customScroll">
          <table>
            <tr>
              <th>S.no</th>
              <th>Product name</th>
              <th>MRP</th>
              <th>Quantity</th>
              <th>Amount</th>
            </tr>
            {retailerLocation.map((item) => (
              <tr>
                <td>{item.id + 1} </td>
                <td>{item.productName}</td>
                <td>{item.mrp}</td>
                <td>{item.quantity}</td>
                <td>{item.amount}</td>
              </tr>
            ))}
            <tr className="total-txt pr-5">
              <td colspan="4" className="lable p-2">
                Total amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              </td>
              <td>Rs.250</td>
            </tr>
          </table>
        </div>
      </>
    );
  };

  const viewRetailModal = () => {
    setViewRetailPopup(true);
  };

  const assignRetailContent = () => {
    return (
      <>
        <div class="main search retail popupSearch">
          <div class="form-group has-search">
            <span class="form-control-feedback">
              <FaSearch />
            </span>
            <input
              type="text"
              class="form-control"
              placeholder="Search by retailor name"
            />
          </div>
        </div>

         
        <div className="assain_retail ">
          <MainTable data={data1} columns={columns1}  popup={true} hidePagination={true}/>
          {/* <TableSelect
              clickSelect="true"
              data={tableList}
              offset={filter ? offset : offset}
              // resetSelectRow={resetSelectRow}
              ref={tableRef}
              tableCount={tableCount}
              showAllRecords={showAllRecords}
              showButton={showButton}
              // columns={columnss}
              // filters={tableFilters}
              // getSelectedRow={getSelectedRow}
              searchBar="false"
            /> */}
        </div>
      </>
    );
  };
  return (
    <div className="mt-3 mb-3 order">
      <div className="order-title mb-2">Upcoming orders</div>
      {data.map((data) => {
        return (
          <Card className="mb-3">
            {data.returId === "" ? (
              <Card.Header className={"card-bg2"}>
                <Row>
                  <Col md={3} className="order-id">
                    Order ID : R0001
                  </Col>
                  <Col className="order-text">
                    Order Date & Time: 30 Feb 2022, 2.00 pm
                  </Col>
                </Row>
              </Card.Header>
            ) : (
              <Card.Header className={"card-bg2"}>
                <Row>
                  <Col md={3} className="order-id">
                    Return ID : R0001
                  </Col>

                  <Col md={3} className="order-id">
                    Order ID : #12345
                  </Col>
                  <Col className="order-text">
                    Order Date & Time:30 Feb 2022, 2.00 pm
                  </Col>
                </Row>
              </Card.Header>
            )}

            <Card.Body>
              <Card.Title></Card.Title>
              <Card.Text>
                <div className="card-content">
                  <div className="content-left">
                    <ul>
                      <li>Customer name: Raj</li>
                      <li>Mobile: 99429129763</li>
                      <li>Email Id: Raj@gmail.com</li>
                      <li>Total amount: 300</li>
                    </ul>
                  </div>
                  <div className="text-center content-right">
                    <p>Prescription</p>
                    <img src={Prescription} alt="prescription" />
                    <p className="prescription">View prescription</p>
                    <p
                      className="view-order-txt color-text"
                      onClick={viewRetailModal}
                    >
                      View order{" "}
                    </p>
                  </div>
                </div>
                {data.returId === "" ? (
                  <Row className="justify-content-md-center  ">
                    <Col
                      xs
                      lg="2"
                      sm="12"
                      md="3"
                      className=" text-center pl-2 m-2"
                    >
                      <Button bg=" denyHoverBg w-100" name="Deny" />
                    </Col>

                    <Col
                      xs
                      lg="2"
                      md={{ span: 3, offset: 0 }}
                      sm="8"
                      className="split d-flex justify-content-center align-items-center text-center mb-3 assignHoverBg p-0   m-2"
                    >
                      <div className=" d-flex justify-content-between align-items-center w-100 ">
                        <div
                          className="split-btn  w-100"
                          onClick={() => assignRetailModal()}
                        >
                          Assign
                        </div>
                      </div>
                      <div className="line-btn"> &nbsp;</div>
                      <span
                        onClick={() => setManualAssign(!manualAssign)}
                        className="d-flex justify-content-center split-icon "
                      >
                        {manualAssign ? (
                          <BiChevronUp size={29} />
                        ) : (
                          <BiChevronDown size={29} />
                        )}
                      </span>
                      {manualAssign && (
                        <div className="splitTop text-center">
                          Assign manually
                        </div>
                      )}
                    </Col>

                    <Col
                      xs
                      lg="2"
                      md={{ span: 3, offset: 0 }}
                      sm="8"
                      className="text-center mb-3 m-2"
                    >
                      <Button bg="approveHoverBg w-100" name="Approve" />
                    </Col>
                  </Row>
                ) : (
                  <Row className="justify-content-md-center  ">
                    <Col xs lg="2" sm="12" md="3" className=" mb-3 text-center">
                      <Button name="Deny" bg="pl-3 pr-3 w-100 denyHoverBg" />
                    </Col>

                    <Col xs lg="2" md="3" sm="8" className="text-center">
                      <Button name="Approve" bg="w-100 approveHoverBg" />
                    </Col>
                  </Row>
                )}
              </Card.Text>
            </Card.Body>
          </Card>
        );
      })}{" "}
      {viewRetailPopup && (
        <ViewModalPopup
          close={setViewRetailPopup}
          popup={viewRetailPopup}
          heading={"View order"}
          body={bodyContent()}
          size={"lg"}
        />
      )}
      {assignRetail && (
        <ViewModalPopup
          close={setAssignRetail}
          popup={assignRetail}
          heading={"Assign retail"}
          body={assignRetailContent()}
          size={"lg"}
        />
      )}
    </div>
  );
}

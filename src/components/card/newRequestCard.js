import React from "react";
import { Card, Row, Col } from "react-bootstrap";
import Prescription from "../../assests/prescription.png";
import GstImage from "../../assests/gst.png";
import DrugImage from "../../assests/drug.png";
import Retail from "../../assests/retail.png";
import Driver from "../../assests/driver.png";
import Button from "../button/button";
import Dropdown from "react-bootstrap/Dropdown";
import DeliveryImage from "../../assests/delivery.png";
import { useState } from "react";
import { NavLink } from "react-router-dom";

export default function NewRequestCard() {
  const [manualAssign, setManualAssign] = useState(false);
  const [data, setData] = useState([
    { type: "Retail store", orderId: 123234 },
    { type: "Delivery partner", orderId: 123234 },
  ]);

  return (
    <div className="mt-3 mb-3 order">
      <div className="order-title mb-2">New user request</div>
      {data.map((data) => {
        return (
          <Card className="mb-3">
            <Card.Header className={"card-bg2"}>
              <Row>
                <Col>
                  <div className="d-flex align-items-center">
                    {data.type === "Delivery" ? (
                      <img src={DeliveryImage} alt="prescription" />
                    ) : (
                      <img src={Retail} alt="prescription" />
                    )}
                    <span className="color-text  p-3">{data.type}</span>
                  </div>
                </Col>
                <Col className="order-text">
                  Requested Date & Time: 30 Feb 2022, 2.00 pm
                </Col>
              </Row>
            </Card.Header>

            <Card.Body>
              <Card.Title></Card.Title>
              <Card.Text>
                <div
                  className="card-content
"
                >
                  <div className="content-left">
                    {data.type === "Delivery partner" ? (
                      <ul>
                        <li>Name: Raj</li>
                        <li>Mobile: 99429129763</li>
                        <li>Email Id: Raj@gmail.com</li>
                      </ul>
                    ) : (
                      <ul>
                        <li>Pharmacy name: ABC pharmacy</li>
                        <li>Competent name: Susan</li>
                        <li>Mobile: 99429129763</li>
                        <li>Email Id: Raj@gmail.com</li>
                      </ul>
                    )}
                  </div>
                  <div className="text-center content-right">
                    <p>Documents</p>
                    {data.type === "Delivery partner" ? (
                      <div className="drug-img">
                        <img src={Driver} alt="prescription" />
                        <p className="prescription pt-1"> Driver licence</p>
                      </div>
                    ) : (
                      <div className="user">
                        <div className="d-flex flex-column justify-content-center align-items-center drug-img">
                          <img src={GstImage} alt="prescription" />
                          <p className="prescription pt-1">GSTIN copy</p>
                        </div>
                        <div className="d-flex flex-column justify-content-center align-items-center drug-img ">
                          <img src={DrugImage} alt="prescription" />
                          <p className="prescription pt-1"> Drug licence</p>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                {data.returId === "" ? (
                  <Row className="justify-content-md-center  ">
                    {/* <Col
                      xs
                      lg="2"
                      sm="12"
                      md="3"
                      className=" text-center mb-3"
                    >
                      <Button bg="bg-red" name="Deny" />
                    </Col>

                    <Col xs lg="2" md="3" sm="8" className="text-center mb-3">
                      <Button bg="bg-green" name="Approve" />
                    </Col> */}
                  </Row>
                ) : (
                  <Row className="justify-content-md-center  ">
                    <Col
                      xs
                      lg="2"
                      sm="12"
                      md="3"
                      className=" mb-3 text-center"
                    >
                      <Button name="Deny" bg="w-100 denyHoverBg" />
                    </Col>

                    <Col xs lg="2" md="3" sm="8" className="text-center">
                      <Button name="Approve" bg="w-100 approveHoverBg" />
                    </Col>
                  </Row>
                )}
              </Card.Text>
            </Card.Body>
          </Card>
        );
      })}{" "}
    </div>
  );
}

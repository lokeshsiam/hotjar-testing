import React from 'react';
import {Button} from "react-bootstrap"

export default function Buttons({ variant,name,bg,onClick}) {

  return (
    <Button className={bg ? bg : ""} active={false} onClick={onClick}>
      {name}
    </Button>
  );
}

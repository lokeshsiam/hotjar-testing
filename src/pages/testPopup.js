import React, { useState } from "react";
import { Button, Modal, Table, Form, Col, Row } from "react-bootstrap";
import ViewModalPopup from "../components/modalPopup";
import Buttons from "../components/button/button";

import GST from "../assests/gst.png";
import Medical from "../assests/Medical.png";
import addFile from "../assests/Addfile.png";
import deliveryLicense from "../assests/Driver_license.png";
import LogoutModalPopup from "../components/logoutPopup";
import { IoCloseOutline } from "react-icons/io5";
import SimpleMap from "./map";

export default function Dashboard() {
  const [showPopup, setShowPopup] = useState(false);
  const [viewRetailPopup, setViewRetailPopup] = useState(false);
  const [retailStorePopup, setRetailStorePopup] = useState(false);
  const [lable, setLable] = useState();
  // const [editRetailStorePopup,setEditRetailStorePopup] = useState (false);
  const [addRetailStorePopup, setAddRetailStorePopup] = useState(false);

  const [viewDeliveryPopup, setViewDeliveryPopup] = useState(false);
  const [addDeliveryPopup, setAddDeliveryPopup] = useState(false);

  const [viewTicketPopup, setViewTicketPopup] = useState(false);
  const [logoutPopup, setLogoutPopup] = useState(false);

  // const [isOpenCounts, setIsOpenCounts] = useState (false);

  const [mapPopup, setMapPopup] = useState(false);

  const retailerLocation = [
    {
      id: 0,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 1,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 2,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 3,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
  ];

  const handleModal = () => {
    setShowPopup(true);
  };
  const viewRetailModal = () => {
    setViewRetailPopup(true);
  };
  const viewRetailStoreModal = (name) => {
    setRetailStorePopup(true);
    setLable(name);
  };
  // const editRetailStoreModal = () => {
  //   setEditRetailStorePopup (true);
  // }
  const addRetailStoreModal = () => {
    setAddRetailStorePopup(true);
  };
  const viewDeliveryPartnerModal = (name) => {
    setViewDeliveryPopup(true);
    setLable(name);
  };
  const addDeliveryPartnerModal = () => {
    setAddDeliveryPopup(true);
  };
  const viewTicketModal = () => {
    setViewTicketPopup(true);
  };
  const logoutModal = () => {
    setLogoutPopup(true);
  };
  const mapModal = () => {
    setMapPopup(true);
  };
  /*------------view order------------------- */
  const bodyContent = () => {
    return (
      <>
        <div className="d-flex justify-content-between mr-3 ml-3 mb-1">
          <p className="color-text order-text"> Order ID : 1001 </p>
          <p className="order-date"> Order Date and Time:10 jan 2022,2:00pm </p>
        </div>
        <div className="d-flex justify-content-between retailStore-detail  ml-3 mb-3">
          <p className="order-date">
            {" "}
            Customer name: <span> Raj </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Mobile: <span> 9898978938 </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Email: <span> raj@email.com </span>{" "}
          </p>
        </div>
        <div className="d-flex justify-content-between mr-3 ml-3 mb-5 product-details">
          <table>
            <tr>
              <th>S.No</th>
              <th>Product Name</th>
              <th>MRP</th>
              <th>Quantity</th>
              <th>Amount</th>
            </tr>
            {retailerLocation.map((item) => (
              <tr>
                <td>{item.id + 1} </td>
                <td>{item.productName}</td>
                <td>{item.mrp}</td>
                <td>{item.quantity}</td>
                <td>{item.amount}</td>
              </tr>
            ))}
            <tr className="total-txt">
              <td colspan="4">Total</td>
              <td>$250</td>
            </tr>
          </table>
        </div>
      </>
    );
  };

  /*------------------------view Retail content---------------------------------- */
  const viewRetailContent = (name) => {
    return (
      <>
        <div className="ml-3 mb-1">
          <p className="retailStore-text"> ID : R0001 </p>
        </div>
        <Form className="ml-3 mb-1">
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Pharmacy name
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="ABC Pharmacy"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Competent name
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Susan"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Address
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="No:14 nth street , adyar , chennai"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Email
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="raj@mail.com"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Mobile number
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="989989898"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Documents
            </Form.Label>
            <div>
              <Col
                sm="0"
                className="d-flex flex-column justify-content-center icon-box p-2 ml-3"
              >
                <span>
                  {" "}
                  <img src={GST} alt="GST" className="ml-2" />{" "}
                </span>
                <small> GSTIN copy </small>
              </Col>
              <small className="text-danger text-center remove-txt">
                {" "}
                {name === "Edit" && <>Remove</>}
              </small>
            </div>
            <div>
              <Col
                sm="0"
                className="d-flex flex-column justify-content-center icon-box ml-4 p-2"
              >
                <span>
                  {" "}
                  <img src={Medical} alt="Drug" className="ml-3" />{" "}
                </span>
                <small> Drug license </small>
              </Col>
              <small className="text-danger text-center remove-txt">
                {" "}
                {name === "Edit" && <>Remove</>}
              </small>
            </div>
          </Form.Group>
        </Form>
        <div className="d-flex justify-content-end ">
          {name === "Add" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel"
                onClick={() => setRetailStorePopup(false)}
                name="Cancel"
              />
              <Buttons bg="bg-red m-4 save" name={name} />
            </>
          )}
          {name === "View" && (
            <Buttons
              bg="bg-red m-4 save"
              name="close"
              onClick={() => setRetailStorePopup(false)}
            />
          )}
          {name === "Edit" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel"
                onClick={() => setRetailStorePopup(false)}
                name="Cancel"
              />
              <Buttons bg="bg-red m-4 save" name="Update" />
            </>
          )}
        </div>
      </>
    );
  };

  /*--------------------Add Retail Content------------------------- */
  const addRetailContent = () => {
    return (
      <>
        <Form className="ml-3 mb-1">
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Pharmacy name
            </Form.Label>
            <Col sm="7">
              <Form.Control type="text" />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Competent name
            </Form.Label>
            <Col sm="7">
              <Form.Control type="text" />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Email
            </Form.Label>
            <Col sm="7">
              <Form.Control type="text" />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Mobile number
            </Form.Label>
            <Col sm="7">
              <Form.Control type="text" />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Documents
            </Form.Label>
            <div>
              <Col
                sm="0"
                className="d-flex flex-column justify-content-center icon-box p-2 ml-3"
              >
                <span>
                  {" "}
                  <img src={addFile} alt="GST" className="ml-3" />{" "}
                </span>
                <small> GSTIN copy </small>
              </Col>

              <div>
                <label htmlFor="upload-button">
                  <>
                    <small className="color-text text-center remove-txt">
                      {" "}
                      Add file{" "}
                    </small>
                  </>
                </label>
                <input
                  type="file"
                  id="upload-button"
                  style={{ display: "none" }}
                  // onChange={handleChange}
                />
                <br />
              </div>
            </div>
            <div className="d-flex flex-column justify-content-center">
              <Col
                sm="0"
                className="d-flex flex-column justify-content-center icon-box ml-4 p-2 mr-5"
              >
                <span>
                  {" "}
                  <img src={addFile} alt="Drug" className="ml-3" />{" "}
                </span>
                <small> Drug license </small>
              </Col>

              <div>
                <label htmlFor="upload-button">
                  <>
                    <small className="color-text text-center remove-txt ml-5">
                      {" "}
                      Add file{" "}
                    </small>
                  </>
                </label>
                <input
                  type="file"
                  id="upload-button"
                  style={{ display: "none" }}
                  // onChange={handleChange}
                />
                <br />
              </div>
            </div>
          </Form.Group>
        </Form>
        <Button className="float-right p-2 pl-5 pr-5 mb-3 ml-3 close-btn-bgColor">
          {" "}
          Next{" "}
        </Button>
      </>
    );
  };

  /*-------------------View Delivery Content---------------------------- */
  const viewDeliveryPartnerContent = (name) => {
    return (
      <>
        <Row className="ml-0 mb-1 mt-2">
          <Col>
            {" "}
            <p className="retailStore-text"> ID : R0001 </p>{" "}
          </Col>
          <Col sm="9">
            {" "}
            <p className="retailStore-text"> Status: Active </p>{" "}
          </Col>
        </Row>
        <Form className="ml-3 mb-1">
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Name
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Raj"
                className="input-background"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              City
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Chennai"
                className="input-background"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Mobile number
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="989989898"
                className="input-background"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Email
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="raj@mail.com"
                className="input-background"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Documents
            </Form.Label>
            <div>
            <Col
              sm="0"
              className="d-flex flex-column justify-content-center icon-box p-2 ml-3"
            >
              <span>
                {" "}
                <img src={deliveryLicense} alt="GST" className="ml-4" />{" "}
              </span>
              <small> DeliveryLicense </small>
            </Col>
            <span>
            <small className="text-danger text-center remove-txt ml-5">
                {name === "Edit" && <>Remove</>}
              </small>
            </span>
              </div>
          </Form.Group>
        </Form>
        <div className="d-flex justify-content-end ">
          {name === "Add" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel"
                onClick={() => setRetailStorePopup(false)}
                name="Cancel"
              />
              <Buttons bg="bg-red m-4 save" name={name} />
            </>
          )}
          {name === "View" && (
            <Buttons
              bg="m-4 save"
              name="Close"
              onClick={() => setRetailStorePopup(false)}
            />
          )}
          {name === "Edit" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel"
                onClick={() => setRetailStorePopup(false)}
                name="Cancel"
              />
              <Buttons bg="m-4 save" name="Update" />
            </>
          )}
        </div>
      </>
    );
  };

  /*--------------------Add Delivery Partner Content--------------------------- */
  const addDeliveryPartnerContent = () => {
    return (
      <>
        <Row className="ml-0 mb-1 mt-2">
          <Col>
            {" "}
            <p className="retailStore-text"> ID : R0001 </p>{" "}
          </Col>
          <Col sm="9">
            {" "}
            <p className="retailStore-text"> Status: Active </p>{" "}
          </Col>
        </Row>
        <Form className="ml-3 mb-1">
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Name
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Raj"
                className="input-background"
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              City
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Chennai"
                className="input-background"
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Mobile number
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="989989898"
                className="input-background"
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Email
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="raj@mail.com"
                className="input-background"
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Documents
            </Form.Label>
            <div>
              <Col
                sm="0"
                className="d-flex flex-column justify-content-center icon-box p-2 ml-3"
              >
                <span>
                  {" "}
                  <img src={addFile} alt="GST" className="ml-4" />{" "}
                </span>
                <small> Driving license </small>
              </Col>
              <small className="color-text text-center remove-txt">
                {" "}
                Add file{" "}
              </small>
            </div>
          </Form.Group>
        </Form>
        <Button className="float-right p-2 pl-5 pr-5 mb-5 ml-3 close-btn-bgColor">
          {" "}
          Add{" "}
        </Button>
        <Button className="float-right p-2 pl-5 pr-5 mb-5 bg-white text-dark border cancel-txt">
          {" "}
          Cancel{" "}
        </Button>
      </>
    );
  };

  /*---------------------View Ticket Content------------------------------ */
  const viewTicketContent = () => {
    return (
      <>
        <div className="d-flex justify-content-between mr-3 ml-3 mb-1">
          <p className="color-text order-text"> ID: T0001 </p>
          <p className="order-date"> Ticket raised date : 15 Feb2022 </p>
        </div>
        <div className="d-flex justify-content-between retailStore-detail  ml-3 mb-3">
          <p className="order-date">
            {" "}
            Customer name: <span> Raj </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Mobile: <span> 9898978938 </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Email: <span> raj@email.com </span>{" "}
          </p>
        </div>
        <Form className="ml-3 mb-1">
          <Form.Group
            as={Row}
            className="mb-3 ml-0"
            controlId="formPlaintextPassword"
          >
            <Form.Label sm="3" className="retailStore-text">
              Product name
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Product 1"
                className="input-background"
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3 ml-0"
            controlId="formPlaintextPassword"
          >
            <Form.Label sm="3" className="retailStore-text">
              Description
            </Form.Label>
            <Col sm="7" className="ml-4">
              <Form.Control
                as="textarea"
                rows={5}
                className="input-background text-area-font-size"
                placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi sse 1"
              />
            </Col>
          </Form.Group>
        </Form>
        <Button className="float-right p-2 pl-5 pr-5 mb-5 mt-4 mr-3 close-btn-bgColor">
          {" "}
          Update{" "}
        </Button>
      </>
    );
  };

  /*---------------------Log out content------------------------ */
  const logoutContent = () => {
    return (
      <>
        <IoCloseOutline
          className="float-right mb-5"
          onClick={() => {
            setLogoutPopup(false);
          }}
        />
        <p className="mb-4 mt-3"> Are you sure you want to log out? </p>
        <div className="d-flex justify-content-center ">
          <Button
            variant="primary"
            size="sm"
            className="p-1 pr-4 pl-4 bg-red logout-btns"
          >
            No
          </Button>
          <Button
            variant="secondary"
            size="sm"
            className="p-1 pr-4 pl-4 bg-green logout-btns ml-5"
          >
            Yes
          </Button>
        </div>
      </>
    );
  };

  /*--------------------Map Content------------------------- */



  const mapContent = () => {
    return (
      <>
        <div className=" Map-content ">
          <SimpleMap
            buttonname={"Add and Proceed"}
            name={"popupMap"}
            size={"modal-70w"}
          />
        </div>
      </>
    );
  };


  return (
    <div>
      {/* <Button onClick={handleModal}> Assign Modal </Button>
    {showPopup && <ModalPopup close={setShowPopup} popup={showPopup} heading={"Assign retail"}/>} */}

      <Button onClick={viewRetailModal} style={{ marginLeft: 20 }}>
        {" "}
        View order{" "}
      </Button>
      {viewRetailPopup && (
        <ViewModalPopup
          close={setViewRetailPopup}
          popup={viewRetailPopup}
          heading={"View order"}
          body={bodyContent()}
          size={"lg"}
        />
      )}

      <Button
        onClick={() => viewRetailStoreModal("View")}
        style={{ marginLeft: 20 }}
      >
        View retail store{" "}
      </Button>
      {retailStorePopup && (
        <ViewModalPopup
          close={setRetailStorePopup}
          popup={retailStorePopup}
          heading={`${lable} retail store`}
          body={viewRetailContent(lable)}
          size={"lg"}
        />
      )}

      <Button
        onClick={() => viewRetailStoreModal("Edit")}
        style={{ marginLeft: 20 }}
      >
        Edit retail store{" "}
      </Button>
      {/* {editRetailStorePopup && <ViewModalPopup close={setEditRetailStorePopup} popup={editRetailStorePopup} heading={"Edit retail store"} body ={editRetailContent()} size={"lg"}/>} */}

      <Button onClick={addRetailStoreModal} style={{ marginLeft: 20 }}>
        Add retail store{" "}
      </Button>
      {addRetailStorePopup && (
        <ViewModalPopup
          close={setAddRetailStorePopup}
          popup={addRetailStorePopup}
          heading={"Add retail store"}
          body={addRetailContent()}
          size={"lg"}
        />
      )}

      <Button onClick={() => viewDeliveryPartnerModal("View")} style={{ marginLeft: 20 }}>
        View delivery partner{" "}
      </Button>
      {viewDeliveryPopup && (
        <ViewModalPopup
          close={setViewDeliveryPopup}
          popup={viewDeliveryPopup}
          heading={`${lable} delivery partner`}
          body={viewDeliveryPartnerContent(lable)}
          size={"lg"}
        />
      )}
       <Button onClick={() => viewDeliveryPartnerModal("Edit")} style={{ marginLeft: 20 }}>
        Edit delivery partner{" "}
      </Button>

      <Button
        onClick={addDeliveryPartnerModal}
        style={{ marginLeft: 20 }}
        className="mt-4"
      >
        Add delivery partner{" "}
      </Button>
      {addDeliveryPopup && (
        <ViewModalPopup
          close={setAddDeliveryPopup}
          popup={addDeliveryPopup}
          heading={"Add delivery partner"}
          body={addDeliveryPartnerContent()}
          size={"lg"}
        />
      )}

      <Button
        onClick={viewTicketModal}
        style={{ marginLeft: 20 }}
        className="mt-4"
      >
        View Ticket
      </Button>
      {viewTicketPopup && (
        <ViewModalPopup
          close={setViewTicketPopup}
          popup={viewTicketPopup}
          heading={"View Ticket"}
          body={viewTicketContent()}
          size={"lg"}
        />
      )}

      <Button onClick={mapModal} style={{ marginLeft: 20 }} className="mt-4">
        Map
      </Button>
      {mapPopup && (
        <LogoutModalPopup
          close={setMapPopup}
          popup={mapPopup}
          heading={"View Ticket"}
          body={mapContent()}
          size={"lg"}
        />
      )}
      {/* <Button
        onClick={logoutModal}
        style={{ marginLeft: 20 }}
        className="mt-4 bg-danger"
      >
        Log Out
      </Button>
      {logoutPopup && (
        <LogoutModalPopup
          close={setLogoutPopup}
          popup={logoutPopup}
          body={logoutContent()}
          size={"sm"}
        />
      )} */}
    </div>
  );
}

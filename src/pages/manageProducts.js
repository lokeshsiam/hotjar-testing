import React, { useState } from "react";
import MainTable from "../components/tables/mainTable";
import { FaSearch } from "react-icons/fa";
import Buttons from "../components/button/button";
import { Button, Form, Row, Col } from "react-bootstrap";
import { BsFillEyeFill } from "react-icons/bs";
import { FaPen } from "react-icons/fa";
import { IoMdCloudUpload } from "react-icons/io";
import ViewModalPopup from "../components/modalPopup";
import { NavLink, Link } from "react-router-dom";

function RetailStore() {
  /*---------------------Add-product------------------*/
  const [addCategory, setAddCategory] = useState(false);
  const [addCoupon, setAddCoupon] = useState(false);
  const [addProduct, setAddProduct] = useState(false);
  const [lable, setLable] = useState();
  const [charCount, setCharCount] = useState(240);
  const [maxLength, setMaxLength] = useState(240);

  const addProductModal = (name) => {
    setAddProduct(true);
    setLable(name);
  };

  const Handler = (e) => {
    var lenCount = e.target.value.length;
    var max_Len = maxLength;
    var result = max_Len - lenCount;
    setCharCount(result);
  };

  const addProductContent = (name) => {
    return (
      <Form>
        <Form.Group as={Row} className="mb-3 mb-1">
          <Form.Label column sm="3">
            Product name:
          </Form.Label>
          <Col sm="7">
            <Form.Control
              type="text"
              placeholder=""
              disabled={name === "View"}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-3 mb-1">
          <Form.Label column sm="3">
            Select category:
          </Form.Label>
          <Col sm="7">
            <Form.Select className="select-category" disabled={name === "View"}>
              <option></option>
            </Form.Select>
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-1 ">
          <Form.Label column sm="3">
            Descriptions:
          </Form.Label>
          <Col sm="7">
            <Form.Group
              className="mb-0"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Control
                as="textarea"
                rows={3}
                disabled={name === "View"}
                onChange={Handler}
              />
            </Form.Group>
            <p className="float-right mt-0">{charCount}/240 </p>
          </Col>
        </Form.Group>

        <Form.Group as={Row} className=" mb-3">
          <Form.Label column sm="3">
            Other comments:
          </Form.Label>

          <Col sm="7">
            <Form.Control
              type="text"
              placeholder=""
              disabled={name === "View"}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className=" mb-3 ">
          <Form.Label column sm="3">
            Price(MRP):
          </Form.Label>
          <Col sm="7">
            <Form.Control
              type="text"
              placeholder=""
              disabled={name === "View"}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className=" mb-3 upload ">
          <Form.Label column sm="3">
            Add image:
          </Form.Label>
          <Col
            className="ml-4"
            sm="3"
            className={
              "image"
                ? "imgUploadBox d-flex align-items-center justify-content-center mwx-400"
                : "imgUploadBox d-flex align-items-center justify-content-center mwx-400 ml-auto mr-auto "
            }
          >
            <input
              type="file"
              name="avatar"
              accept="image/*"
              disabled={name === "View"}
            />
            <IoMdCloudUpload />
          </Col>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Check type="checkbox" label="Prescription required" />
        </Form.Group>

        <div className="d-flex justify-content-end modalBtnHoverBg">
          {name === "Add" && (
            <>
              {" "}
              <Buttons
                bg=" m-4  cancel edit-cancel"
                onClick={() => setAddCategory(false)}
                name="Cancel"
              />
              <Buttons bg=" m-4 save" name="Add" />
            </>
          )}
          {name === "View" && (
            <>
              {" "}
              <Buttons bg=" m-4 save" name="close" />
            </>
          )}

          {name === "Edit" && (
            <>
              {" "}
              <Buttons
                bg="m-4  cancel edit-cancel"
                onClick={() => setAddCategory(false)}
                name="Cancel"
              />
              <Buttons bg=" m-4 save edit-save" name="Update" />
            </>
          )}
        </div>
      </Form>
    );
  };

  const head = "Retail Store";
  const buttonhead = "Payment Completed";
  const data = [
    {
      s_no: 1,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 2,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 3,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 4,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 5,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 6,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 7,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 8,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 9,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 10,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 11,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 12,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 13,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 14,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
    {
      s_no: 15,
      products_name: "Product 1",
      category_name: "Category 1",
      image: <Link className="image">Image1.png</Link>,
      price: "100",
    },
  ];

  const columns = [
    {
      dataField: "s_no",
      text: "S.no",
    },
    {
      dataField: "products_name",
      text: "Products name",
    },
    {
      dataField: "category_name",
      text: "Category name",
    },
    {
      dataField: "image",
      text: "Image",
    },
    {
      dataField: "price",
      text: "Price(MRP)",
    },

    {
      dataField: "action",
      text: "Action",
      formatter: userButtonFormatter,
    },
  ];

  function userButtonFormatter(cell, row) {
    return (
      <>
        <div className="d-flex justify-content-center align-items-center ">
          <Button
            className="border-none pr-2"
            variant="link"
            dataid={cell}
            onClick={() => addProductModal("View")}
          >
            <BsFillEyeFill />
          </Button>
          {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
          <Button
            className="border-none"
            variant="link"
            dataid={cell}
            onClick={() => addProductModal("Edit")}
          >
            <FaPen />{" "}
          </Button>
          <div class="custom-control custom-switch toggle-switch">
            <input
              type="checkbox"
              class="custom-control-input"
              id="customSwitches"
            />
            <label class="custom-control-label" for="customSwitches"></label>
          </div>
        </div>
      </>
    );
  }
  return (
    <>
      <div className="p-3 ">
        <div className="retailHeading ">
          <p>Product</p>
        </div>
      </div>
      <div className="search-button-table  mt-3 ml-4 m-3">
        <div className=" d-flex justify-content-between search-border mainButtonHoverBg">
          <div class="main search retail ">
            <div class="form-group has-search pt-4">
              <span class="form-control-feedback">
                <FaSearch size={20} />
              </span>
              <input type="search" class="form-control" placeholder="Search" />
            </div>
          </div>

          <Buttons
            bg="bg-white mt-3 mr-2 w-60 font"
            name="Add Product"
            onClick={() => addProductModal("Add")}
          />
        </div>
        <MainTable data={data} columns={columns} />

        {addProduct && (
          <ViewModalPopup
            close={setAddProduct}
            popup={addProduct}
            heading={`${lable} product`}
            body={addProductContent(lable)}
            size={"lg"}
          />
        )}
      </div>
    </>
  );
}
export default RetailStore;

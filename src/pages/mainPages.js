import React, { Component, useState, useRef } from "react";
import MainTable from "../components/tables/mainTable";

import { FaSearch } from "react-icons/fa";
import { Button, Form, Row, Col } from "react-bootstrap";
import Buttons from "../components/button/button";

import { FaPen } from "react-icons/fa";
import { BsFillEyeFill } from "react-icons/bs";
import addFile from "../assests/Addfile.png";
import Medical from "../assests/Medical.png";
import GST from "../assests/gst.png";
import ViewModalPopup from "../components/modalPopup";

function RetailStore() {
  const tableRef = useRef(null);

  const head = "Retail Store";
  const buttonhead = "Payment Completed";

  const [addRetailStorePopup, setAddRetailStorePopup] = useState(false);
  const [retailStorePopup, setRetailStorePopup] = useState(false);
  const [lable, setLable] = useState();
  const [count, setCount] = useState(0);

  const addRetailStoreModal = () => {
    setAddRetailStorePopup(true);
  };

  const viewRetailStoreModal = (name) => {
    setRetailStorePopup(true);
    setLable(name);
  };

  /*--------------------Add Retail Content------------------------- */
  const addRetailContent = () => {
    return (
      <>
        <Form className="ml-3 mb-1">
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Pharmacy name
            </Form.Label>
            <Col sm="7">
              <Form.Control type="text" />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Competent name
            </Form.Label>
            <Col sm="7">
              <Form.Control type="text" />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Email
            </Form.Label>
            <Col sm="7">
              <Form.Control type="text" />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Mobile number
            </Form.Label>
            <Col sm="7">
              <Form.Control type="text" />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Documents
            </Form.Label>
            <div>
              <Col
                sm="0"
                className="d-flex flex-column justify-content-center icon-box p-2 ml-3"
              >
                <label htmlFor="upload-button" className="w-100">
                  {" "}
                  <img src={addFile} alt="GST" className="ml-3 cursor-pointer" />{" "}
                </label>
                <small> GSTIN copy </small>
              </Col>

              <div>
                <label htmlFor="upload-button">
                  <>
                    <small className="color-text text-center remove-txt">
                      {" "}
                      Add file{" "}
                    </small>
                  </>
                </label>
                <input
                  type="file"
                  id="upload-button"
                  style={{ display: "none" }}
                  // onChange={handleChange}
                />
                <br />
              </div>
            </div>
            <div className="d-flex flex-column justify-content-center">
              <Col
                sm="0"
                className="d-flex flex-column justify-content-center icon-box ml-4 p-2 mr-5"
              >
                <label htmlFor="upload-button">
                  {" "}
                  <img src={addFile} alt="Drug" className="ml-3 cursor-pointer" />{" "}
                </label>
                <small> Drug license </small>
              </Col>

              <div>
                <label htmlFor="upload-button">
                  <>
                    <small className="color-text text-center remove-txt ml-5">
                      {" "}
                      Add file{" "}
                    </small>
                  </>
                </label>
                <input
                  type="file"
                  id="upload-button"
                  style={{ display: "none" }}
                  // onChange={handleChange}
                />
                <br />
              </div>
            </div>
          </Form.Group>
        </Form>
        <Button className="float-right p-2 pl-5 pr-5 mb-3 ml-3 close-btn-bgColor modalNextBtnHoverBg ">
          {" "}
          Next{" "}
        </Button>
      </>
    );
  };

  /*------------------------view Retail content---------------------------------- */
  const viewRetailContent = (name) => {
    return (
      <>
        <div className="ml-3 mb-1">
          <p className="retailStore-text"> ID : R0001 </p>
        </div>
        <Form className="ml-3 mb-1">
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Pharmacy name
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="ABC Pharmacy"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Competent name
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Susan"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Address
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="No:14 nth street , adyar , chennai"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Email
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="raj@mail.com"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Mobile number
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="989989898"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Documents
            </Form.Label>
            <div>
              <Col
                sm="0"
                className="d-flex flex-column justify-content-center icon-box p-2 ml-3"
              >
                <span>
                  {" "}
                  <img src={GST} alt="GST" className="ml-2" />{" "}
                </span>
                <small> GSTIN copy </small>
              </Col>
              <small className="text-danger text-center remove-txt">
                {" "}
                {name === "Edit" && <>Remove</>}
              </small>
            </div>
            <div>
              <Col
                sm="0"
                className="d-flex flex-column justify-content-center icon-box ml-4 p-2"
              >
                <span>
                  {" "}
                  <img src={Medical} alt="Drug" className="ml-3" />{" "}
                </span>
                <small> Drug license </small>
              </Col>
              <small className="text-danger text-center remove-txt">
                {" "}
                {name === "Edit" && <>Remove</>}
              </small>
            </div>
          </Form.Group>
        </Form>
        <div className="d-flex justify-content-end modalBtnHoverBg">
          {name === "Add" && (
            <>
              {" "}
              <Buttons
                bg=" m-4  cancel"
                onClick={() => setRetailStorePopup(false)}
                name="Cancel"
              />
              <Buttons bg="bg-red m-4 save" name={name} />
            </>
          )}
          {name === "View" && (
            <Buttons
              bg=" m-4 save"
              name="Close"
              onClick={() => setRetailStorePopup(false)}
            />
          )}
          {name === "Edit" && (
            <>
              <Buttons bg=" m-4 save" name="Update" />
            </>
          )}
        </div>
      </>
    );
  };

  function userButtonFormatter(cell, row) {
    return (
      <>
        <div className="d-flex align-items-center ">
          <Button
            className="border-none pr-2"
            variant="link"
            dataid={cell}
            onClick={() => viewRetailStoreModal("View")}
          >
            <BsFillEyeFill />
          </Button>
          {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
          <Button
            className="border-none"
            variant="link"
            dataid={cell}
            onClick={() => viewRetailStoreModal("Edit")}
          >
            <FaPen />{" "}
          </Button>

          <div class="custom-control custom-switch toggle-switch">
            <input
              type="checkbox"
              class="custom-control-input"
              id="customSwitches"
            />
            <label class="custom-control-label" for="customSwitches"></label>
          </div>
        </div>
      </>
    );
  }

  const data = [
    {
      s_no: 1,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 2,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 3,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 4,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 5,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 6,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 7,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 8,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 9,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 10,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 11,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 12,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 13,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 14,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
    {
      s_no: 15,
      id: "R00001",
      pharmacy_name: "ABC Pharmacy",
      component_name: "Susan",
      mobile_num: 9876543210,
      e_mail: "susan@mail.com",
      address: "Adayar,Chennai",
    },
  ];
  const columns = [
    {
      dataField: "s_no",
      text: "S.no",
    },
    {
      dataField: "id",
      text: "ID",
    },
    {
      dataField: "pharmacy_name",
      text: "Pharmacy name",
    },
    {
      dataField: "component_name",
      text: "Component name",
    },
    {
      dataField: "mobile_num",
      text: "Mobile number",
    },
    {
      dataField: "e_mail",
      text: "Email",
    },
    {
      dataField: "address",
      text: "Address",
    },
    {
      dataField: "action",
      text: "Action",
      formatter: userButtonFormatter,
    },
  ];

  const value = [
    {
      email: "leo9@yopmail.com",
      first_name: "John Smith",
    },
    {
      email: "leo9@yopmail.com",
      first_name: "John Smith",
    },
  ];

  const columnss = [
    { dataField: "sno", text: "S.No" },
    {
      dataField: "first_name",
      text: "Username",
    },
    { dataField: "email", text: "Email ID" },
  ];
  const getSelectedRow = (data) => {};
  return (
    <>
      <div className="p-3">
        <div className="retailHeading ">
          <p>Retail store</p>
        </div>
        <div className="search-button-table  mt-5  m-2 ">
          <div className=" searchbox d-flex justify-content-between search-border mainButtonHoverBg">
            <div class="main search retail ">
              <div class="form-group has-search pt-4 ">
                <span class="form-control-feedback">
                  <FaSearch size={20} />
                </span>
                <input
                  type="search"
                  class="form-control"
                  placeholder="Search"
                />
              </div>
            </div>

            <Buttons
              bg="bg-white mt-3 mr-2 w-60 font"
              name="Add retail store"
              onClick={() => addRetailStoreModal("Add")}
              // className="add_retail_btn"
            />
          </div>
          <div className="manage">
            <MainTable
              data={data}
              columns={columns}
              setPage={setCount}
              className="mt-5"
            />
          </div>
        </div>

        {addRetailStorePopup && (
          <ViewModalPopup
            close={setAddRetailStorePopup}
            popup={addRetailStorePopup}
            heading={"Add retail store"}
            body={addRetailContent()}
            size={"lg"}
          />
        )}

        {retailStorePopup && (
          <ViewModalPopup
            close={setRetailStorePopup}
            popup={retailStorePopup}
            heading={`${lable} retail store`}
            body={viewRetailContent(lable)}
            size={"lg"}
          />
        )}
      </div>
    </>
  );
}
export default RetailStore;

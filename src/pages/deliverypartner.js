import React, { Component, useState } from "react";
import MainTable from "../components/tables/mainTable";
import { FaSearch } from "react-icons/fa";
import { Button, Modal, Table, Form, Col, Row } from "react-bootstrap";
import ViewModalPopup from "../components/modalPopup";
import { BsFillEyeFill } from "react-icons/bs";
import Buttons from "../components/button/button";
import { FaPen } from "react-icons/fa";
import addFile from "../assests/Addfile.png";
import deliveryLicense from "../assests/Driver_license.png";

function DeliveryPartner() {
  const [addDeliveryPopup, setAddDeliveryPopup] = useState(false);
  const [viewDeliveryPopup, setViewDeliveryPopup] = useState(false);
  const [lable, setLable] = useState();
  
  
  const addDeliveryPartnerModal = (name) => {
    setAddDeliveryPopup(true);
    setLable(name);
  };
  const viewDeliveryPartnerModal = (name) => {
    setViewDeliveryPopup(true);
    setLable(name);
  };

  

  /*--------------------Add Delivery Partner Content--------------------------- */
  const addDeliveryPartnerContent = (name) => {
    return (
      <>
        <Row className="ml-0 mb-1 mt-2">
          <Col>
            {" "}
            <p className="retailStore-text"> ID : R0001 </p>{" "}
          </Col>
          <Col sm="9">
            {" "}
            <p className="retailStore-text"> Status: Active </p>{" "}
          </Col>
        </Row>
        <Form className="ml-3 mb-1">
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Name
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Raj"
                className="input-background"
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              City
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Chennai"
                className="input-background"
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Mobile number
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="989989898"
                className="input-background"
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Email
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="raj@mail.com"
                className="input-background"
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Documents
            </Form.Label>
            <div>
              <Col
                sm="0"
                className="d-flex flex-column justify-content-center icon-box p-2 ml-3"
              >
                <label htmlFor="upload-button">
                  {" "}
                  <img src={addFile} alt="GST" className="ml-4 cursor-pointer" />{" "}
                </label>
                <small> Driving license </small>
              </Col>

              <div>
                <label htmlFor="upload-button">
                  <>
                    <small className="color-text text-center remove-txt pl-2 bg-">
                      {" "}
                      Add file{" "}
                    </small>
                  </>
                </label>
                <input
                  type="file"
                  id="upload-button"
                  style={{ display: "none" }}
                  // onChange={handleChange}
                />
                <br />
              </div>

            </div>
          </Form.Group>
        </Form>
        
        <div className="d-flex justify-content-end modalBtnHoverBg">
          {name === "Add" && (
            <>
              {" "}
              <Buttons
                bg=" m-4  cancel edit-cancel"
                // onClick={() => setAddCategory(false)}
                name="Cancel"
              />
              <Buttons bg=" m-4 save" name="Add" />
            </>
          )}
          {name === "View" && (
            <>
              {" "}
              <Buttons bg=" m-4 save " name="close" />
            </>
          )}

          {name === "Edit" && (
            <>
              {" "}
              <Buttons
                bg="m-4  cancel edit-cancel"
                // onClick={() => setAddCategory(false)}
                name="Cancel"
              />
              <Buttons bg=" m-4 save edit-save" name="Update" />
            </>
          )}
        </div>
      </>
    );
  };

   /*-------------------View Delivery Content---------------------------- */
   const viewDeliveryPartnerContent = (name) => {
    return (
      <>
        <Row className="ml-0 mb-1 mt-2">
          <Col>
            {" "}
            <p className="retailStore-text"> ID : R0001 </p>{" "}
          </Col>
          <Col sm="9">
            {" "}
            <p className="retailStore-text"> Status: Active </p>{" "}
          </Col>
        </Row>
        <Form className="ml-3 mb-1">
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Name
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Raj"
                className="input-background"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              City
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Chennai"
                className="input-background"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Mobile number
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="989989898"
                className="input-background"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Email
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="raj@mail.com"
                className="input-background"
                disabled={name === "View"}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formPlaintextPassword"
          >
            <Form.Label column sm="3" className="retailStore-text">
              Documents
            </Form.Label>
            <div>
            <Col
              sm="0"
              className="d-flex flex-column justify-content-center icon-box p-2 ml-3"
            >
              <span>
                {" "}
                <img src={deliveryLicense} alt="GST" className="ml-4" />{" "}
              </span>
              <small> Driver license </small>
            </Col>
            <span>
            <small className="text-danger text-center remove-txt ml-5">
                {name === "Edit" && <>Remove</>}
              </small>
            </span>
              </div>
          </Form.Group>
        </Form>
        <div className="d-flex justify-content-end modalBtnHoverBg">
          {name === "Add" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4 cancel "
                onClick={() => setViewDeliveryPopup(false)}
                name="Cancel"
              />
              <Buttons bg="bg-red m-4 save " name={name} />
            </>
          )}
          {name === "View" && (
            <Buttons
              bg="m-4 save"
              name="Close"
              onClick={() => setViewDeliveryPopup(false)}
            />
          )}
          {name === "Edit" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel edit-cancel"
                // onClick={() => setRetailStorePopup(false)}
                name="Cancel"
              />
              <Buttons bg="m-4 save edit-save" name="Update" />
            </>
          )}
        </div>
      </>
    );
  };

  function userButtonFormatterView(cell, row) {
    return (
      <>
        <div className="d-flex justify-content-center align-items-center">
          <Button
            className="border-none pr-2"
            variant="link"
            dataid={cell}
            onClick={() => viewDeliveryPartnerModal("View")}
          >
            <BsFillEyeFill />
          </Button>
          {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
          <Button
            className="border-none"
            variant="link"
            dataid={cell}
            onClick={() => viewDeliveryPartnerModal("Edit")}
          >
            <FaPen />{" "}
          </Button>

          <div class="custom-control custom-switch toggle-switch">
            <input
              type="checkbox"
              class="custom-control-input"
              id="customSwitches"
            />
            <label class="custom-control-label" for="customSwitches"></label>
          </div>
        </div>
      </>
    );
  }

  const data = [
    {
      s_no: 1,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 2,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Inactive",
    },
    {
      s_no: 3,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 4,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Inactive",
    },
    {
      s_no: 5,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Pending",
    },
    {
      s_no: 6,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 7,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 8,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 9,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 10,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 11,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 12,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 13,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 14,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
    {
      s_no: 15,
      id: "R00001",
      name: "Raj",
      mobile_num: 9876543210,
      e_mail: "raj@mail.com",
      address: "Adayar,chennai",
      status: "Active",
    },
  ];
  const columns = [
    {
      dataField: "s_no",
      text: "S.no",
    },
    {
      dataField: "id",
      text: "ID",
    },
    {
      dataField: "name",
      text: "Name",
    },
    ,
    {
      dataField: "mobile_num",
      text: "Mobile number",
    },
    {
      dataField: "e_mail",
      text: "Email",
    },
    {
      dataField: "address",
      text: "Address",
    },
    {
      dataField: "status",
      text: "Status",
    },
    {
      dataField: "action",
      text: "Action",
      formatter: userButtonFormatterView,
    },
  ];

  return (
    <>
      <div className="p-3">
        <div className="retailHeading ">
          <p>Delivery Partner</p>
        </div>

        <div className="search-button-table mt-5 m-2 ">
          <div className=" d-flex justify-content-between search-border mainButtonHoverBg">
            <div class="main search retail ">
              <div class="form-group has-search pt-4">
                <span class="form-control-feedback">
                  <FaSearch size={20} />
                </span>
                <input
                  type="search"
                  class="form-control"
                  placeholder="Search"
                />
              </div>
            </div>

            <Buttons
              bg="bg-white mt-3 mr-2 w-60 font"
              name="Add delivery partner"
              onClick={() => addDeliveryPartnerModal("Add")}
            />
          </div>

          <div className="manage">
            <MainTable data={data} columns={columns} />
          </div>
        </div>
        
        {addDeliveryPopup && (
          <ViewModalPopup
            close={setAddDeliveryPopup}
            popup={addDeliveryPopup}
            heading={"Add delivery partner"}
            body={addDeliveryPartnerContent(lable)}
            size={"lg"}
          />
        )}
        {viewDeliveryPopup && (
        <ViewModalPopup
          close={setViewDeliveryPopup}
          popup={viewDeliveryPopup}
          heading={`${lable} delivery partner`}
          body={viewDeliveryPartnerContent(lable)}
          size={"lg"}
        />
      )}
      </div>
    </>
  );
}
export default DeliveryPartner;
